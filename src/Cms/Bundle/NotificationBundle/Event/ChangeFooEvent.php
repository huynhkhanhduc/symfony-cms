<?php 

namespace Cms\Bundle\NotificationBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class ChangeFooEvent extends Event
{
    protected $foo;

    public function __construct($foo)
    {
        $this->foo = $foo;
    }

    public function getFoo()
    {
        return $this->foo;
    }
    public function onFooAction(Event $event)
    {
      echo 'ChangeFooEvent' ;  
    }
}