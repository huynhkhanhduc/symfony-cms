<?php
namespace Cms\Bundle\NotificationBundle\Event;
use Cms\Bundle\NotificationBundle\Event\ChangeFooEvent;
use Symfony\Component\EventDispatcher\Event;

class NotificationListener extends Event
{
    // ...

    public function onFooAction(ChangeFooEvent $event)
    {
        // ... do something
         echo '<br/>ChangeFooEvent Cms\Bundle\NotificationBundle\Event\NotificationListener <br/>' ;  
         // die;
    }
}