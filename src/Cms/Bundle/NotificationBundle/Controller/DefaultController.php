<?php

namespace Cms\Bundle\NotificationBundle\Controller;

use Cms\Bundle\NotificationBundle\Controller\NotificationController;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cms\Bundle\NotificationBundle\Event\ChangeFooEvent;
use Cms\Bundle\NotificationBundle\Event\NotificationListener;
use Symfony\Component\EventDispatcher\EventDispatcher;
// use Cms\Bundle\NotificationBundle\Controller\Notification;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
    	
    	$notification = $this->get('cms_notification.example');
        $notification->set('foo', 'Foo Change');
        $foo = $notification->get('foo');

        $notification->set('bar', 'Bar Change');
        $bar = $notification->get('bar');

        // echo $bar."=>".$foo;
        $dispatcher =$this->get('event_dispatcher');

        $listener = new NotificationController();
        $dispatcher->addListener('foo.change', array($listener, 'onFooChange'));

        $listener = new NotificationListener();
        $dispatcher->addListener('foo.change', array($listener, 'onFooAction'));
        
		// create the FilterOrderEvent and dispatch it
		$event = new ChangeFooEvent($foo);
		$dispatcher->dispatch('foo.change', $event);

        return $this->render('CmsNotificationBundle:Default:index.html.twig', array('name' => $name));
    }
    private function registerEvent(){

    }
}
