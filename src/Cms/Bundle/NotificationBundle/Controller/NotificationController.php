<?php

namespace Cms\Bundle\NotificationBundle\Controller;

use Cms\Bundle\NotificationBundle\Event\ChangeFooEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NotificationController extends Controller
{
	protected $foo;
	protected $bar;
	protected $lipsum;

	public function get($k){
		return $this->$k;
	}    
	public function set($k,$v){
		// the order is somehow created or retrieved
		// $notication = new NotificationListener();
		// ...

		// create the FilterOrderEvent and dispatch it
		// $event = new FilterOrderEvent($order);
		// $dispatcher->dispatch(StoreEvents::STORE_ORDER, $event);

		$this->$k = $v;
	}


	public function onFooChange(ChangeFooEvent $event)
	{
	    $foo = $event->getFoo();
	    // do something to or with the order
	    echo 'Foo Changed Trigger';
	    // die;
	}
}
