<?php 
// src/AppBundle/Menu/Builder.php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        
        // $menu->addChild('Home', array('route' => 'homepage'));
        $menu->addChild('Blog', array('route' => 'blog'));
        $menu->addChild('Comment', array('route' => 'comment'));
        $menu->addChild('Category', array('route' => 'category'));
        // $menu->addChild('DashBoard', array('route' => '#'));

        // // access services from the container!
        // $em = $this->container->get('doctrine')->getManager();
        // // findMostRecent and Blog are just imaginary examples
        // $blog = $em->getRepository('AppBundle:BlogPost')->findMostRecent();

        // $menu->addChild('Latest Blog Post', array(
        //     'route' => 'blog_show',
        //     'routeParameters' => array('id' => $blog->getId())
        // ));

        // // create another menu item
        // $menu->addChild('About Me', array('route' => 'about'));
        // // you can also add sub level's to your menu's as follows
        // $menu['About Me']->addChild('Edit profile', array('route' => 'edit_profile'));

        // ... add more children

        return $menu;
    }
}