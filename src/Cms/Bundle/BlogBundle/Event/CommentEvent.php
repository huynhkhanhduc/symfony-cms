<?php
namespace Cms\Bundle\BlogBundle\Event;
use Symfony\Component\EventDispatcher\Event;

class CommentEvent extends Event{

    protected $comments;

    function __construct($comments)
    {
        $this->comments = $comments;
    }

    public function setComments($comments){
        $this->comments = $comments;
    }
    public function getComments(){
        return $this->comments;
    }


}