<?php
namespace Cms\Bundle\BlogBundle\Event;


final class CommentConstant{

    const COMMENT_LIST = "cms.comment.listing";
    const COMMENT_DETAIL = "cms.comment.detail";
    const COMMENT_SAVE = "cms.comment.save";

}