<?php

namespace Cms\Bundle\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BlogPost
 */
class BlogPost
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $content;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Cms\Bundle\BlogBundle\Entity\BlogCategory
     */
    private $category;


    /**
     * Set title
     *
     * @param string $title
     * @return BlogPost
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return BlogPost
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return BlogPost
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param \Cms\Bundle\BlogBundle\Entity\BlogCategory $category
     * @return BlogPost
     */
    public function setCategory(\Cms\Bundle\BlogBundle\Entity\BlogCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Cms\Bundle\BlogBundle\Entity\BlogCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }
     /**
    * Return Title
    **/
    public function __toString(){
        return $this->title;
    }
}
