<?php

namespace Cms\Bundle\BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cms\Bundle\BlogBundle\Event\CommentEvent;
use Cms\Bundle\BlogBundle\Event\CommentListener;
use Cms\Bundle\BlogBundle\Event\CommentConstant;

use Cms\Bundle\BlogBundle\Entity\BlogComment;
use Cms\Bundle\BlogBundle\Entity\CommentModel;
use Cms\Bundle\BlogBundle\Form\BlogCommentType;

use Cms\Bundle\BlogBundle\DependencyInjection\CategoryExtension;




/**
 * BlogComment controller.
 *
 */
class BlogCommentController extends Controller
{

    /**
     * Lists all BlogComment entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $blogEntity = new BlogComment();
        
        // $entities = $em->getRepository('CmsBlogBundle:BlogComment')->findAll();
        $model = new CommentModel($em,'CmsBlogBundle:BlogComment');
        // $entities = $model->findAll();
        $entities = $model->getRepository()->findAll();

        $cmsService = $this->get('cms_blogbundle.service');
        $start = $cmsService->start();
        // Get Post Extension 
        $postExtension = $this->get('cms.postextension');

        $categoryExtension = new CategoryExtension("CategoryExtension Title");
        $postExtension->setCategory($categoryExtension);
        $postExtensionCategory = $postExtension->getCategory();
        // var_dump($postExtensionCategory->);die;
        
        /** @var EventDispatcher $eventDispatcher */
        $eventDispatcher = $this->get('event_dispatcher');
        $event = new CommentEvent($entities);
        $eventDispatcher->dispatch(CommentConstant::COMMENT_LIST, $event);

        return $this->render('CmsBlogBundle:BlogComment:index.html.twig', array(
            'entities' => $entities,
            'start' => $start,
            'postExtensionCategory' => $postExtensionCategory->getTitle(),
        ));
    }
    /**
     * Creates a new BlogComment entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new BlogComment();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('comment_show', array('id' => $entity->getId())));
        }

        return $this->render('CmsBlogBundle:BlogComment:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a BlogComment entity.
     *
     * @param BlogComment $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(BlogComment $entity)
    {
        $form = $this->createForm(new BlogCommentType(), $entity, array(
            'action' => $this->generateUrl('comment_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new BlogComment entity.
     *
     */
    public function newAction()
    {
        $entity = new BlogComment();
        $form   = $this->createCreateForm($entity);

        return $this->render('CmsBlogBundle:BlogComment:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a BlogComment entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CmsBlogBundle:BlogComment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlogComment entity.');
        }
        $eventDispatcher = $this->get('event_dispatcher');
        $event = new CommentEvent($entity);
        $eventDispatcher->dispatch(CommentConstant::COMMENT_DETAIL, $event);

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CmsBlogBundle:BlogComment:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing BlogComment entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CmsBlogBundle:BlogComment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlogComment entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CmsBlogBundle:BlogComment:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a BlogComment entity.
    *
    * @param BlogComment $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(BlogComment $entity)
    {
        $form = $this->createForm(new BlogCommentType(), $entity, array(
            'action' => $this->generateUrl('comment_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing BlogComment entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CmsBlogBundle:BlogComment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlogComment entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('comment_edit', array('id' => $id)));
        }

        return $this->render('CmsBlogBundle:BlogComment:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a BlogComment entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CmsBlogBundle:BlogComment')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find BlogComment entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('comment'));
    }

    /**
     * Creates a form to delete a BlogComment entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('comment_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
