<?php

namespace Cms\Bundle\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('CmsBlogBundle:Default:index.html.twig', array('name' => $name));
    }
}
