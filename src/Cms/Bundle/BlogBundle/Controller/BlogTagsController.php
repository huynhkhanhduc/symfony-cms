<?php

namespace Cms\Bundle\BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cms\Bundle\BlogBundle\Entity\BlogTags;
use Cms\Bundle\BlogBundle\Form\BlogTagsType;

/**
 * BlogTags controller.
 *
 */
class BlogTagsController extends Controller
{

    /**
     * Lists all BlogTags entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CmsBlogBundle:BlogTags')->findAll();

        return $this->render('CmsBlogBundle:BlogTags:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new BlogTags entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new BlogTags();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tags_show', array('id' => $entity->getId())));
        }

        return $this->render('CmsBlogBundle:BlogTags:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a BlogTags entity.
     *
     * @param BlogTags $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(BlogTags $entity)
    {
        $form = $this->createForm(new BlogTagsType(), $entity, array(
            'action' => $this->generateUrl('tags_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new BlogTags entity.
     *
     */
    public function newAction()
    {
        $entity = new BlogTags();
        $form   = $this->createCreateForm($entity);

        return $this->render('CmsBlogBundle:BlogTags:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a BlogTags entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CmsBlogBundle:BlogTags')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlogTags entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CmsBlogBundle:BlogTags:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing BlogTags entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CmsBlogBundle:BlogTags')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlogTags entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CmsBlogBundle:BlogTags:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a BlogTags entity.
    *
    * @param BlogTags $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(BlogTags $entity)
    {
        $form = $this->createForm(new BlogTagsType(), $entity, array(
            'action' => $this->generateUrl('tags_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing BlogTags entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CmsBlogBundle:BlogTags')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlogTags entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tags_edit', array('id' => $id)));
        }

        return $this->render('CmsBlogBundle:BlogTags:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a BlogTags entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CmsBlogBundle:BlogTags')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find BlogTags entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tags'));
    }

    /**
     * Creates a form to delete a BlogTags entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tags_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
