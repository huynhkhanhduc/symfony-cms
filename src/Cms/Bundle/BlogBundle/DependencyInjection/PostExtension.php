<?php

namespace Cms\Bundle\BlogBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class PostExtension
{
   private $title ="Post";
   private $category;
   public function setCategory(CategoryExtension $category)
    {
        $this->category = $category;
    }
    public function getCategory(){
    	return $this->category;
    }
}
